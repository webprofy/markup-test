var argv = require('yargs').argv;

module.exports = {
	paths: {
		base: './',
		img: '../img/',
		js: '../js/',
		localjs: './js/',
		css: '../css/',
		fonts: '../fonts/',
		sass: './sass/',
		blocks: './blocks/',
		helpers: './sass/helpers/',
		pages: './pages/',
		tmp: './tmp/',
		localimg: './img/',
		localfonts: './fonts/'
	},

	localPaths: {
		base: './public/',
		img: './public/img/',
		js: './public/js/',
		localjs: './js/',
		css: './public/css/',
		fonts: './public/fonts/',
		sass: './sass/',
		blocks: './blocks/',
		tmp: './tmp/',
		helpers: './sass/helpers/',
		pages: './pages/',
		localimg: './img/',
		localfonts: './fonts/'
	},

	browsers: [
		'ie >= 8',
		'ff >= 29',
		'Opera >= 12',
		'iOS >= 6',
		'Chrome >= 28',
		'Android >= 2'
	],

	env: {
		isFast: (argv.fast !== undefined || argv.page !== undefined),
		isLocal: (argv.local !== undefined || argv.production === undefined),
		isProduction: (argv.production !== undefined),
		watchPage: argv.page
	}
};
