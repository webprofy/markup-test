// jQuery Mobile Zoom by Denis Korovkin

$(function(){
    $.fn.mobilezoom = function(options) {
        var settings = $.extend({
            backbutton: 'Вернуться',
            marginbetween: 30
        }, options );

        var touches = [];
        var zoomStartTouches = [];
        var zoomStarted = false;
        var zoomStartDistance = false;
        var zoomStartSize = false;
        var moveStarted = false;
        var moveStartPosition = false;

        var next = false;
        var prev = false;

        var animating = false;

        /* If many elements — do for each */
        if(this.length > 1){
            this.each(function(){
                $(this).mobilezoom(settings);
            });
            return this;
        }
        /* If no elements — exit */
        if(this.length == 0){
            return this;
        }

        /* If already initialized — exit */
        if(this.is('.mobilezoom__element')) return this;

        this.addClass('mobilezoom__element');
        initTouchEvents();

        /* If is gallery — get other elements */
        if(this.data('gallery')){
            var gallery = $('*[data-gallery="'+ this.data('gallery') +'"]');
        }

        var current = this;

        /* Handle click event */
        this.on('click', function(e){
            showImage(this);
            e.preventDefault();
            return false;
        });

        function showImage(element){
            element = $(element);
            var url;
            if(element.is('img')){
                url = element.attr('src');
            } else {
                url = element.attr('href');
            }
            $('body').append('<div class="mobilezoom__holder"><div class="mobilezoom__topbar"><div class="mobilezoom__backbutton">'+settings.backbutton+'</div></div><div class="mobilezoom__image"><img src="'+ url +'" /></div></div>');
            $('body').addClass('mobilezoom__opened');

            window.mobileZoomGallery = gallery;
            window.mobileZoomCurrent = element;

            defaultImagePosition();

            $('.mobilezoom__backbutton').on('click', function(e){
                $('.mobilezoom__holder').remove();
                $('body').removeClass('mobilezoom__opened');
            });
            $('.mobilezoom__holder').on('click', function(e){
                $('.mobilezoom__topbar').fadeToggle(300);
            });

            prepareNext();
        }

        function showNext(element){
            element = $(element);
            var url;
            if(element.is('img')){
                url = element.attr('src');
            } else {
                url = element.attr('href');
            }
            $('.mobilezoom__holder').append('<div class="mobilezoom__next-image"><img src="'+ url +'" /></div>');

            function setPosition(){
                var width = $('.mobilezoom__next-image').find('img').width();
                var height = $('.mobilezoom__next-image').find('img').height();
                var naturalWidth = $('.mobilezoom__next-image').find('img').get(0).naturalWidth;
                var naturalHeight = $('.mobilezoom__next-image').find('img').get(0).naturalHeight;

                if(naturalWidth > 0){
                    $('.mobilezoom__next-image').data('nwidth', naturalWidth).data('nheight', naturalHeight);
                    $('.mobilezoom__next-image').width('width').height('height').css({
                        'left': ($('.mobilezoom__holder').width() + 20) + 'px',
                        'top': (($('.mobilezoom__holder').height() - height) / 2) + 'px',
                        'position': 'absolute'
                    });
                } else {
                    $('.mobilezoom__next-image').find('img').on('load', setPosition);
                }
            }
            setPosition();

        }

        function showPrev(element){
            element = $(element);
            var url;
            if(element.is('img')){
                url = element.attr('src');
            } else {
                url = element.attr('href');
            }
            $('.mobilezoom__holder').append('<div class="mobilezoom__prev-image"><img src="'+ url +'" /></div>');

            function setPosition(){
                var width = $('.mobilezoom__prev-image').find('img').width();
                var height = $('.mobilezoom__prev-image').find('img').height();
                var naturalWidth = $('.mobilezoom__prev-image').find('img').get(0).naturalWidth;
                var naturalHeight = $('.mobilezoom__prev-image').find('img').get(0).naturalHeight;

                if(naturalWidth > 0){
                    $('.mobilezoom__prev-image').data('nwidth', naturalWidth).data('nheight', naturalHeight);
                    $('.mobilezoom__prev-image').width('width').height('height').css({
                        'left': (- $('.mobilezoom__holder').width() - settings.marginbetween) + 'px',
                        'top': (($('.mobilezoom__holder').height() - height) / 2) + 'px',
                        'position': 'absolute'
                    });
                } else {
                    $('.mobilezoom__prev-image').find('img').on('load', setPosition);
                }
            }
            setPosition();
        }

        function prepareNext(){

            var gallery = window.mobileZoomGallery;
            var current = window.mobileZoomCurrent;

            if(!gallery) {
                return;
            }

            var current_index = gallery.index(current);

            if(current_index < gallery.length - 1){
                next = gallery.eq(current_index + 1);
                showNext(next);
            } else {
                next = false;
            }
            if(current_index > 0){
                prev = gallery.eq(current_index - 1);
                showPrev(prev);
            } else {
                prev = false;
            }
        }

        function defaultImagePosition(){
            var width = $('.mobilezoom__image').find('img').width();
            var height = $('.mobilezoom__image').find('img').height();
            var naturalWidth = $('.mobilezoom__image').find('img').get(0).naturalWidth;
            var naturalHeight = $('.mobilezoom__image').find('img').get(0).naturalHeight;

            if(naturalWidth > 0){
                $('.mobilezoom__image').data('nwidth', naturalWidth).data('nheight', naturalHeight);
                $('.mobilezoom__image').width('width').height('height').css({
                    'left': 0,
                    'top': (($('.mobilezoom__holder').height() - height) / 2) + 'px'
                });
            } else {
                $('.mobilezoom__image').find('img').on('load', defaultImagePosition);
            }
        }

        function initTouchEvents(){
            if($('body').is('.mobilezoom__ready')) return;

            $('body').addClass('mobilezoom__ready');

            touches[0] = {x: false, y: false};
            touches[1] = {x: false, y: false};
            touches[99] = {x: false, y: false}; // that's middle point

            $(document).on('touchstart', function(e){
                touches[0] = {x: e.originalEvent.touches[0].clientX, y: e.originalEvent.touches[0].clientY};
                touches[99] = touches[0];

                if(e.originalEvent.touches.length > 1){
                    touches[1] = {x: e.originalEvent.touches[1].clientX, y: e.originalEvent.touches[1].clientY};
                    touches[99] = {
                        x: (touches[0].x + touches[1].x) / 2,
                        y: (touches[0].y + touches[1].y) / 2
                    }
                } else {
                    touches[1] = {x: false, y: false};
                }

            });
            $(document).on('touchend', function(e){
                if(e.originalEvent.touches.length == 0){
                    touches[0] = {x: false, y: false};
                    touches[1] = {x: false, y: false};
                    touches[99] = {x: false, y: false};

                    if(moveStarted){
                        fitBounds();
                    }

                    zoomStarted = false;
                    moveStarted = false;
                }
            });

            $(document).on('touchmove', function(e){

                if($('body').is('.mobilezoom__opened')){

                    var previous = $.extend({}, touches);
                    var zoomDelta = 0;
                    if(animating) return;
                    if(!moveStarted){
                        moveStartPosition = {
                            x: $('.mobilezoom__image').position().left,
                            y: $('.mobilezoom__image').position().top
                        }
                        moveStarted = true;
                    }

                    touches[0] = {x: e.originalEvent.touches[0].clientX, y: e.originalEvent.touches[0].clientY};
                    touches[99] = touches[0];

                    if(e.originalEvent.touches.length > 1){
                        touches[1] = {x: e.originalEvent.touches[1].clientX, y: e.originalEvent.touches[1].clientY};
                        touches[99] = {
                            x: (touches[0].x + touches[1].x) / 2,
                            y: (touches[0].y + touches[1].y) / 2
                        }

                        if(!zoomStarted){
                            zoomStarted = true;
                            zoomStartTouches = $.extend({}, touches);
                            zoomStartDistance = getZoomDistance();
                            zoomStartSize = {
                                width: $('.mobilezoom__image').width(),
                                height: $('.mobilezoom__image').height(),
                                aspect: $('.mobilezoom__image').data('nwidth') / $('.mobilezoom__image').data('nheight')
                            }
                        } else {
                            zoomDelta = getZoomDistance() - zoomStartDistance;
                        }

                    } else {
                        if(zoomStarted){
                            moveStartPosition = {
                                x: $('.mobilezoom__image').position().left,
                                y: $('.mobilezoom__image').position().top
                            }
                            previous = $.extend({}, touches); // prevent jumping

                        }
                        touches[1] = {x: false, y: false};
                        zoomStarted = false;
                    }

                    var positionDeltaX = touches[99].x - previous[99].x;
                    var positionDeltaY = touches[99].y - previous[99].y;

                    if(zoomStarted) {

                        var zoomDeltaX = touches[99].x - zoomStartTouches[99].x;
                        var zoomDeltaY = touches[99].y - zoomStartTouches[99].y;

                        zoomImage(zoomDelta, zoomDeltaX, zoomDeltaY);
                    } else {
                        moveImage(positionDeltaX, positionDeltaY);
                    }


                    e.preventDefault();
                    return false;
                }
            });

        }

        function getZoomDistance(){
            if(touches[1].x != false){
                return Math.sqrt(Math.pow(touches[0].x - touches[1].x, 2) + Math.pow(touches[0].y - touches[1].y, 2));
            } else {
                return false;
            }
        }

        function moveImage(x, y){
            var left = $('.mobilezoom__image').position().left;
            var top = $('.mobilezoom__image').position().top;
            $('.mobilezoom__image').css({
                'left': (left + x) + 'px',
            });
            if($('.mobilezoom__image').height() > $('.mobilezoom__holder').height()){
                $('.mobilezoom__image').css({
                    'top': (top + y) + 'px'
                });
            }
            $('.mobilezoom__next-image').css({
                'left': ((left + x) + $('.mobilezoom__image').width() + settings.marginbetween) + 'px'
            });
            $('.mobilezoom__prev-image').css({
                'left': ((left + x) - $('.mobilezoom__prev-image').width() - settings.marginbetween) + 'px'
            });
            if($('.mobilezoom__next-image').length == 1 && $('.mobilezoom__next-image').position().left <= $('.mobilezoom__holder').width() / 1.5){
                switchToNext();
            }
            if($('.mobilezoom__prev-image').length == 1 && $('.mobilezoom__prev-image').position().left >= - $('.mobilezoom__holder').width() / 1.5){
                switchToPrev();
            }


        }

        function zoomImage(distance, x, y){
            var xdelta = distance;
            var ydelta = distance / zoomStartSize.aspect;
            if(zoomStartSize.aspect < 1){
                ydelta = distance;
                xdelta = distance * zoomStartSize.aspect;
            }

            // Tweaking
            xdelta*=1.5;
            ydelta*=1.5;

            $('.mobilezoom__image').css({
                'width': (zoomStartSize.width + xdelta) + 'px',
                'height': (zoomStartSize.height + ydelta) + 'px',
                'left': (moveStartPosition.x - xdelta/2 + x) + 'px',
                'top': (moveStartPosition.y - ydelta/2 + y) + 'px'
            });
        }

        function switchToNext(){

            moveStarted = false;
            animating = true;
            var left = $('.mobilezoom__next-image').position().left;

            $('.mobilezoom__next-image').animate({left: 0}, 500);
            $('.mobilezoom__image').animate({left: '-=' + left}, 500, function(){
                $('.mobilezoom__prev-image').remove();
                $('.mobilezoom__image').remove();
                $('.mobilezoom__next-image').removeClass('mobilezoom__next-image').addClass('mobilezoom__image');
                window.mobileZoomCurrent = getNext();
                prepareNext();
                animating = false;
            });
        }

        function getNext(){
            return window.mobileZoomGallery.eq(window.mobileZoomGallery.index(window.mobileZoomCurrent) + 1);
        }

        function getPrev(){
            return window.mobileZoomGallery.eq(window.mobileZoomGallery.index(window.mobileZoomCurrent) - 1);
        }

        function switchToPrev(){
            moveStarted = false;
            animating = true;
            var left = $('.mobilezoom__prev-image').position().left;

            $('.mobilezoom__prev-image').animate({left: 0}, 500);
            $('.mobilezoom__image').animate({left: '+=' + (0-left)}, 500, function(){
                $('.mobilezoom__next-image').remove();
                $('.mobilezoom__image').remove();
                $('.mobilezoom__prev-image').removeClass('mobilezoom__prev-image').addClass('mobilezoom__image');
                window.mobileZoomCurrent = getPrev();
                prepareNext();
                animating = false;
            });
        }

        function fitBounds(){
            var width = $('.mobilezoom__image').width();
            var height = $('.mobilezoom__image').height();
            var nwidth = $('.mobilezoom__image').data('nwidth');
            var nheight = $('.mobilezoom__image').data('nheight');
            var x = $('.mobilezoom__image').position().left;
            var y = $('.mobilezoom__image').position().top;
            var cwidth = $('.mobilezoom__holder').width();
            var cheight = $('.mobilezoom__holder').height();
            var aspect = nwidth/nheight;
            var caspect = cwidth/cheight;

            var maxZoom = 1;

            if(width < cwidth && height < cheight){
                // To small. Fit to window.

                if(aspect > caspect){
                    $('.mobilezoom__image').animate({
                        'width': cwidth + 'px',
                        'height': (cwidth / aspect) + 'px',
                        'left': 0,
                        'top': ((cheight - cwidth / aspect) / 2) + 'px'
                    }, 300);
                } else {
                    $('.mobilezoom__image').animate({
                        'height': cheight + 'px',
                        'width': (cheight * aspect) + 'px',
                        'top': 0,
                        'left': ((cwidth - cheight * aspect) / 2) + 'px'
                    }, 300);
                }
            } else {
                // Fit to bounds
                if(width >= cwidth) {
                    var xnew;
                    if(x > 0) xnew = 0;
                    if(x + width < cwidth) xnew = cwidth - width;
                } else {
                    var xnew = (cwidth - width) / 2;
                }
                if(height >= cheight) {
                    var ynew;
                    if(y > 0) ynew = 0;
                    if(y + height < cheight) ynew = cheight - height;
                } else {
                    var ynew = (cheight - height) / 2;
                }
                if(x != xnew || y != ynew){
                    $('.mobilezoom__image').animate({'top': ynew + 'px', 'left': xnew + 'px'}, 300);
                    $('.mobilezoom__next-image').animate({'left': (cwidth + settings.marginbetween) + 'px'}, 300);
                    $('.mobilezoom__prev-image').animate({'left': (-cwidth - settings.marginbetween) + 'px'}, 300);
                }
            }

            if(width > nwidth * maxZoom || height > nheight * maxZoom){
                // Too big. Zooming to maxZoom
                $('.mobilezoom__image').animate({
                    'width': nwidth * maxZoom + 'px',
                    'height': nheight * maxZoom + 'px',
                    'left': "+=" + (width - nwidth * maxZoom)/2,
                    'top': "+=" + (height - nheight * maxZoom)/2
                }, 300);
            }

        }

    };
});