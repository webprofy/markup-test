// Небольшой jQuery-плагин для адаптации блоков от ширины контекста, а не от ширины экрана
	(function($){
		var aB = $.adaptBlock = {
			_blocks: [],
			_oldWidndowWidth: $(window).width(),
			_newWindowWidth: 0,

			params: {
				resizeTimeout: 80,
				checkClass: 'is-flexible'
			},

			windowResizeHandler: function (e) {
				aB._newWindowWidth = $(window).width();

				if(aB._oldWidndowWidth == aB._newWindowWidth) {
					aB._oldWidndowWidth = aB._newWindowWidth;
					return;
				}

				aB._oldWidndowWidth = aB._newWindowWidth;

				if(aB.resizeTimeout !== undefined) clearTimeout(aB.resizeTimeout);

				aB.resizeTimeout = setTimeout(function() {
					aB.resizeElems();
				}, aB.params.resizeTimeout);
			},

			resizeElems: function () {
				for(var i = 0; i < aB._blocks.length; i++) {
					aB.setMedia(aB._blocks[i]);
				}
			},

			setMedia: function (block) {
				var $el = block.elem;
				var params = block.params;

				if(!$el.is(':visible')) return;

				var width = block.width = $el.parent().width();

				if(width < block.scope.max[0] || width > block.scope.max[1] || !block.scope.maxIsSet) {
					block.scope.max[1] = Infinity;
					block.scope.max[0] = 0;

					$.each(block.maxWidthArray, function (key, value) {
						if(width <= value) {
							$el.addClass(params.maxWidth[value]);
							block.scope.max[1] = Math.min(value, block.scope.max[1]);
						} else {
							$el.removeClass(params.maxWidth[value]);
							block.scope.max[0] = Math.max(value, block.scope.max[0]);
						}
					});
					block.scope.maxIsSet = true;
				}

				$.each(block.minWidthArray, function (key, value) {
					if(width >= value) {
						$el.addClass(params.minWidth[value]);
					} else {
						$el.removeClass(params.minWidth[value]);
					}
				});


				if(block.params.watch) console.log(block);
			},

			initMedia: function (block) {
				var params = block.params;

				if(block.scope == null) {
					block.scope = {
						max: [0, Infinity],
						min: [0, Infinity],
						isSet: false
					}
				}

				$.each(params.maxWidth, function (key, value) {
					block.maxWidthArray.push(parseInt(key));
				});

				$.each(params.minWidth, function (key, value) {
					block.minWidthArray.push(parseInt(key));
				});
			},

			addBlock: function (elem, options) {
				var block = {
					elem: elem,
					params: options,
					maxWidthArray: [],
					minWidthArray: []
				}

				aB._blocks.push(block);
				aB.initMedia(block);
				aB.setMedia(block);

				block.elem.on('resize.block', function (e)  {
					requestAnimFrame(function () { aB.setMedia(block) });
					e.stopPropagation();
				})
			},

			init: function () {
				$(window).on('resize', aB.windowResizeHandler);
			}
		}

		$.fn.adaptBlock = function (options) {
			var params = $.extend({
				maxWidth: {},
				minWidth: {}
			}, options);

			function init() {
				var $elem = $(this);
				if($elem.hasClass(aB.params.checkClass) || $elem.closest('.' + aB.params.checkClass).length > 0) {
					aB.addBlock($(this), params);
				}
			}

			return this.each(init);
		}

		aB.init();
	})(jQuery);
