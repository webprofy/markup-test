// Древовидное меню
	$(function () {
		// Инициализация только для состояния И
		$('.menu-top').livequery(function () {
			var $context = $(this);
			var $firstHolders = $('.menu-top__first-holder', $context);
			var $firstItems = $('.menu-top__first-item', $context);
			var $firstLinks = $('.menu-top__first-link', $context);

			var $secondHolders = $('.menu-top__second-holder', $context);
			var $secondItems = $('.menu-top__second-item', $context);
			var $secondLinks = $('.menu-top__second-link', $context);

			var $thirdHolders = $('.menu-top__third-holder', $context);
			var $thirdItems = $('.menu-top__third-item', $context);
			var $thirdLinks = $('.menu-top__third-link', $context);

			var $fourthHolders = $('.menu-top__fourth-holder', $context);
			var $fourthItems = $('.menu-top__fourth-item', $context);
			var $fourthLinks = $('.menu-top__fourth-link', $context);

			var $allHolders = $firstHolders.add($secondHolders).add($thirdHolders).add($fourthHolders);
			var $allLinks = $firstLinks.add($secondLinks).add($thirdLinks).add($fourthLinks);
			var $allItems = $firstItems.add($secondItems).add($thirdItems).add($fourthItems);

			var $moreLinks = $('.menu-top__more-link', $context);

			function initMoreLinks () {
				$moreLinks.each(function () {
					var $link = $(this);
					var $list = $link.closest('.menu-top__third-level');
					var $items = $list.find('.menu-top__third-item');
					var $less = $list.find('.menu-top__less-link');
					var qtty = $link.data('hide-qtty');

					$less.on('click', function () {
						$items.slice($items.length - (qtty + 1), $items.length - 1).addClass('is-hide');
						$less.removeClass('is-active');
						$link.addClass('is-active');
					});

					$link.on('click', function () {
						$items.removeClass('is-hide');
						$less.addClass('is-active');
						$link.removeClass('is-active');
					});
				});
			}

			initMoreLinks();
		});
	});
